%{
By Maxwell Gentili-Morin

2nd order Sallen Key Anti-Aliasing Filter

V_out/V_in = (b_0)/(a_2*s^2 + a_1*s + a_0)

*****Input Variables*****
Capacitors:
    C1
    C2
Resistors: 
    R1 = R2


b_0 = (C1*C2*R1*R2)^-1
a_2 = 1
a_1 = (C1*R2)^-1 + (C1*R1)^-1 
a_0 = (C1*C2*R1*R2)^-1
%}
function [sd] = s_K_LPF2(C1,C2,R1,R2)
    b_0 = 1/(C1*C2*R1*R2);
    a_2 = 1;
    a_1 = (C1*R2)^-1 + (C1*R1)^-1 ;
    a_0 = (C1*C2*R1*R2)^-1;

    a = [a_2, a_1, a_0];
    b = [b_0];
    
    sc= tf(b,a);
    sd=c2d(sc,1/44100);
    h = bodeplot(sc,"b-",sd,"r--",{1,100000});
    setoptions(h,'FreqUnits','Hz','PhaseVisible','off');
    legend("Continuous-time transfer function", "Discrete-time transfer function")
    grid on
end