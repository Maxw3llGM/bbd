 function [d,freqval] = fftploting(varargin)
    format long
    tiledlayout('flow')
    s = char(97:104);
    ftotal = [];
    dBtotal = [];
    
    for x = varargin
        filetype = string(x);
        if contains(filetype,".wav")
            filename = filetype;
            [ftotal,dBtotal] = freq_analysis(filename);   
        else
                for w = 1:length(s)
                    filename = strcat('../Sounds/',filetype,s(w),'.aif');
                    [f,dB] = freq_analysis(filename);
                    ftotal = [ftotal, f];
                    dBtotal = [dBtotal, dB.'];      
                end
            [ftotal,I] = sort(ftotal);
            dBtotal = dBtotal(I);
        end
        nexttile
        sgf  = sgolayfilt(dBtotal,1,501);
        if contains(filename,"TC")
            [~, ix] = min(abs(ftotal-100));
            mn = sgf(ix);
            %mn = mean(sgf(2000:3600));
            %standard_dev = std(sgf(2000:3600));
        else
            mn = sgf(1);
        end

        sgf =  sgf-mn;
        [d, ix] = min(abs(sgf+3));
        freqval = ftotal(ix);
        semilogx(ftotal,sgf,'Color',[rand,rand,rand]);
        
        xline(freqval)
        yline(-3)
        
        grid on
        xlim([0 max(ftotal)])
        xlabel("Frequency (Hz)")
        ylabel("Magnitude (dB)")
        ftotal = [];
        dBtotal = [];
    end
end