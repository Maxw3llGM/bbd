%{
By Maxwell Gentili-Morin

3rd order Sallen Key Anti-Aliasing Filter

V_out/V_in = (b_0)/(a_3*s^3 + a_2*s^2 + a_1*s + a_0)

*****Input Variables*****
Capacitors:
    C1
    C2
    C3
Resistors: 
    R1 = R2 = R3 = R4


b_0 = (C1*C2*C3*R1*R2*R3)^-1
a_3 = 1
a_2 = (C1*R1)^-1+(C1*R2)^-1+(C2*R3)^-1+(C2*R2)^-1
a_1 = (C2*C3*R2*R3)^-1 + (C1*C2*R2*R3)^-1 + (C1*C2*R1*R3)^-1 + (C1*C2*R1*R2)^-1 
a_0 = (C1*C2*C3*R1*R2*R3)^-1
%}

function [sd] = s_k_LPF(C1, C2, C3, R)
    a_0 = 1/(C1*C2*C3*(R^3));
    a_3 = 1;
    a_2 = 2/(C1*R)+2/(C2*R);
    a_1 = 1/(C2*C3*R^2) + 3/(C1*C2*R^2);
    b_0 = a_0;
    a = [a_3, a_2, a_1, a_0];
    b = [0,0,b_0];
   
    figure(1)           
    sc= tf(b,a);
    sd=c2d(sc,1/44100);  
    h = bodeplot(sc,"b-",sd,"r--",{1,100000});
    setoptions(h,'FreqUnits','Hz','PhaseVisible','off');
    legend("Continuous-time transfer function", "Discrete-time transfer function")
    grid on 
end