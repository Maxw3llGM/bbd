import("stdfaust.lib");

//delay chips
    MN3005(dur,feedback,x) =  (no.noise*0.001)+de.fdelay(maxDel,del,x) * feedback
with{
    maxDel = ma.SR*2;
    minDel = ma.SR*0.02;
    del = ma.SR*dur;
};
//AA Filter
aA3rddorder = fi.iir(bcoeffs,acoeffs)
with{
    bcoeffs = (0,0.00880418916477725,0.0293730739887128,0.00613885692694816);
    acoeffs = (-2.30860041904231,1.83857963415967,-0.485663095036926);
};
//Reconstruction Filters
rec3rddorder = fi.iir(bcoeffs,acoeffs)
with{
    bcoeffs = (0,0.0164607194478194,0.0410398260458653,0.00558035530977087);
    acoeffs = (-1.92386439978695,1.09787491960365,-0.110929619013243);
};
rec2ndorder = fi.iir(bcoeffs,acoeffs)
with{
    bcoeffs = (0,0.185972822723693,0.178809134810342);
    acoeffs = (-1.52543877709347,0.890220734627505);
};
//Main program section
process = +~ ( aA3rddorder : MN3005(duration,feedback) : rec3rddorder : rec2ndorder) <: _,_
with{
    duration = 0.3;
    feedback = 0.17;
};