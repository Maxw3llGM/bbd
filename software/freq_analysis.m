function [f,dB] = freq_analysis(file1)

[x,fs] = audioread(file1);

y = fft(x);

dB = 20*log10(abs(y));
n = length(dB); 
f = (0:n-1)*(fs/n);
f = f(1:floor(n/2+1));
dB = dB(1:floor(n/2+1));
end