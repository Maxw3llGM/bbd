function [a,b] = BBDfilter( C1, C2, C3, C4, C5, C6, C7, C8, R )
% Calculates transfer function given capacitance (uF) and resistance (kOhm)
% for an eighth-order (series 3, 3, 2 order) BBD circuit

% Filters in series have transfer functions that multiply.
% https://ccrma.stanford.edu/~jos/fp/Series_Case.html

% Convert to farads and ohms
C1 = C1*(10^(-6));
C2 = C2*(10^(-6));
C3 = C3*(10^(-6));
C4 = C4*(10^(-6));
C5 = C5*(10^(-6));
C6 = C6*(10^(-6));
C7 = C7*(10^(-6));
C8 = C8*(10^(-6));
R = 1000*R;

% Numerator is 1
b = zeros(1, 9);
b(9) = 1;

% Denonimator is product of two three-pole and one two-pole Sallen-key
% transfer functions
a = zeros(1, 9);
a(9) = 1;

a(8) = (C1+3*C3+C4+3*C6+2*C8)*R;

a(7) = (2*C2*C3+3*C3*C4+9*C3*C6+2*C4*C6+2*C5*C6+6*C3*C8+2*C4*C8+6*C6*C8+ ...
  C7*C8+C1*(2*C3+C4+3*C6+2*C8))*R.^2;

a(6) = (6*C3*C4*C6+6*C3*C5*C6+C4*C5*C6+6*C3*C4*C8+18*C3*C6*C8+4*C4*C6*C8+ ...
  4*C5*C6*C8+3*C3*C7*C8+C4*C7*C8+3*C6*C7*C8+2*C2*C3*(C4+3*C6+2*C8)+ ...
  C1*(C2*C3+2*C4*C6+2*C5*C6+2*C4*C8+6*C6*C8+C7*C8+2*C3*(C4+3*C6+2* ...
  C8)))*R.^3;

a(5) = (3*C3*C4*C5*C6+12*C3*C4*C6*C8+12*C3*C5*C6*C8+2*C4*C5*C6*C8+3*C3* ...
  C4*C7*C8+9*C3*C6*C7*C8+2*C4*C6*C7*C8+2*C5*C6*C7*C8+2*C2*C3*(2*C5* ...
  C6+(6*C6+C7)*C8+2*C4*(C6+C8))+C1*(C4*C5*C6+4*C4*C6*C8+4*C5*C6*C8+ ...
  C4*C7*C8+3*C6*C7*C8+C2*C3*(C4+3*C6+2*C8)+2*C3*(2*C5*C6+6*C6*C8+C7* ...
  C8+2*C4*(C6+C8))))*R.^4;

a(4) = (C6*(C4*C5*C7+6*C3*(C5*C7+C4*(C5+C7)))*C8+2*C2*C3*(C6*(4*C5+3*C7)* ...
  C8+C4*(C5*C6+4*C6*C8+C7*C8))+C1*(C2*C3*(2*C5*C6+6*C6*C8+C7*C8+2* ...
  C4*(C6+C8))+2*(C6*(C5*C7+C4*(C5+C7))*C8+C3*(C6*(4*C5+3*C7)*C8+C4*( ...
  C5*C6+4*C6*C8+C7*C8)))))*R.^5;

a(3) = (C3*C6*(3*C4*C5*C7+4*C2*(C5*C7+C4*(C5+C7)))*C8+C1*(C6*(C4*C5*C7+4* ...
  C3*(C5*C7+C4*(C5+C7)))*C8+C2*C3*(C6*(4*C5+3*C7)*C8+C4*(C5*C6+4*C6* ...
  C8+C7*C8))))*R.^6;

a(2) = 2*C3*C6*(C2*C4*C5*C7+C1*(C4*C5*C7+C2*(C5*C7+C4*(C5+C7))))*C8*R.^7;

a(1) = C1*C2*C3*C4*C5*C6*C7*C8*R.^8;

% Log space for 1hz to 22.5khz
w = logspace( 0, 5.1415, 1000);

% Get frequency response
freqs( b, a, w );



end

