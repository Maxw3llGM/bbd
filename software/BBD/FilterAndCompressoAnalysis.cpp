/*
g++ -std=c++11 -Istk/include/ -Lstk/src/ -D__MACOSX_CORE__ FilterAndCompressoAnalysis.cpp -lstk -lpthread -framework CoreAudio -framework CoreMIDI -framework CoreFoundation
*/

#include "FileWvIn.h"
#include "FileWvOut.h"
#include "RtAudio.h"
#include "Delay.h"
#include "Iir.h"
#include <cstdlib>
#include <cmath>
#include "Noise.h"
#include "Stk.h"

using namespace stk;

Iir *antiAliasFilter;
Iir *reconstructionFilter1;
Iir *reconstructionFilter2;
Iir *compressorAverager;
Iir *expanderAverager;

int main( int argc, char *argv[] )
{
  FileWvIn  input;
  FileWvOut output;

  if ( argc != 2 ) {
    std::cout << "usage: " << argv[0] << " file" << std::endl;
    std::cout << "    where 'file' is an input soundfile to process" << std::endl;
    exit(0);
  }

  try {
    // Load the input file
    input.openFile( argv[1] );
  }
  catch ( StkError & ) {
    exit(0);
  }

  // Set the global STK sample rate to the input file sample rate.
  Stk::setSampleRate( input.getFileRate() );
  StkFloat T = 1/(Stk::sampleRate());

  // Reset the input file reader increment to 1.0
  input.setRate( 1.0 );

  try {
    // Define and open a 16-bit, one-channel WAV formatted output file
    output.openFile( "filter2", 1, FileWrite::FILE_WAV, Stk::STK_SINT16 );
  }
  catch ( StkError & ) {
    input.closeFile();
    exit(0);
  }

  // Coefficients for anti-aliasing filter (calculated in MatLab)
  StkFloat antiAliasBCoefVals[8] = { 0.00002808162654240159, 0.00324260184959550063, 0.01229358405255225224, 0.01781338547909389058, 0.01191661427670600605, 0.00351674566780364349, 0.00034109599239625331, 0.00000665645917792087 };
  StkFloat antiAliasACoefVals[8] = { 1.00000000000000000000, 0.27002485476511250972, -2.35210121584816533868, -0.47624252782494447267, 1.85825180320240179732, 0.30789821629786440216, -0.48145639585378052772, -0.07721597509005941051 };

  std::vector< StkFloat > antiAliasBCoefs( antiAliasBCoefVals, antiAliasBCoefVals+8 );
  std::vector< StkFloat > antiAliasACoefs( antiAliasACoefVals, antiAliasACoefVals+8 );
  // Anti-aliasing filter
  antiAliasFilter = new Iir( antiAliasBCoefs, antiAliasACoefs );

  // Coefficients for reconstruction filter 1 (calculated in MatLab)
  StkFloat reconstruction1BCoefVals[6] = { 0.00051516013318437094, 0.03041821522444834724, 0.06752981661722373685, 0.04770540623300938837, 0.01033396947561467973, 0.00001106873510238544 };
  StkFloat reconstruction1ACoefVals[6] = { 1.00000000000000000000, -0.63300309066683380088, -1.16456643091420186664, 0.76529508607787266605, 0.33776127751256762588, -0.14897208486870763822 };
  std::vector< StkFloat > reconstruction1BCoefs( reconstruction1BCoefVals, reconstruction1BCoefVals+6 );
  std::vector< StkFloat > reconstruction1ACoefs( reconstruction1ACoefVals, reconstruction1ACoefVals+6 );
  // Reconstruction filter 1
  reconstructionFilter1 = new Iir( reconstruction1BCoefs, reconstruction1ACoefs );

  // Coefficients for reconstruction filter 2 (calculated in MatLab)
  StkFloat reconstruction2BCoefVals[3] = { 0.01440296150822061375, 0.12374170373521460597, 0.01322614050481232296 };
  StkFloat reconstruction2ACoefVals[3] = { 1.00000000000000000000, -1.75672492751137410139, 0.90805921207607520618 };
  std::vector< StkFloat > reconstruction2BCoefs( reconstruction2BCoefVals, reconstruction2BCoefVals+3 );
  std::vector< StkFloat > reconstruction2ACoefs( reconstruction2ACoefVals, reconstruction2ACoefVals+3 );
  // Reconstruction filter 2
  reconstructionFilter2 = new Iir( reconstruction2BCoefs, reconstruction2ACoefs );


  ////////////////////////
  // Compander
  ////////////////////////

  // Set default compander cap value in uF
  double companderCrect = .82;
  // Read in compander cap value if it exists
  if ( argc > 5 )
    companderCrect = atof(argv[5]);

  // Get compander rectifier smoothing factor
  // http://en.wikipedia.org/wiki/Low-pass_filter
  StkFloat companderSmoothing = ( T )/( 10000*companderCrect*.000001 + T );
  // Coefficients for averaging filter
  StkFloat averagerBCoefVals[2] = { companderSmoothing, 0.0 };
  StkFloat averagerACoefVals[2] = { 1.0, -1+companderSmoothing };
  std::vector< StkFloat > averagerBCoefs( averagerBCoefVals, averagerBCoefVals+2 );
  std::vector< StkFloat > averagerACoefs( averagerACoefVals, averagerACoefVals+2 );
  // Create compressor averager
  compressorAverager = new Iir( averagerBCoefs, averagerACoefs );
  // Create expander averager
  expanderAverager = new Iir( averagerBCoefs, averagerACoefs );
  StkFloat prevCompOut = 1;
  StkFrames frame( 1, 2 );                     // one frame of 2 channels
  StkFloat previousFrame = 0;
  int i;
  i = input.getSize();  // in sample frames
  while ( i-- >= 0 ) {

    try { // single frame computations
      frame[0] = input.tick();
      frame[1] = (0.5*frame[0] + previousFrame) / (compressorAverager->tick(fabs(prevCompOut)) + 0.00001);
      prevCompOut = frame[1];
      frame[1] = antiAliasFilter->tick( frame[1] );
      frame[1] = reconstructionFilter1->tick(frame[1]);
      frame[1] = reconstructionFilter2->tick(frame[1]);
      //frame[1] = frame[1] * expanderAverager->tick( fabs(frame[1]));
      //previousFrame = frame[1];
      output.tick( frame[1] );
    }
    catch ( StkError & ) {
      goto cleanup;
    }
  }

 cleanup:
  input.closeFile();
  output.closeFile();

  return 0;
}
