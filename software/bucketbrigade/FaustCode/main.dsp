import("stdfaust.lib");
//use ba.downsample to reduce the sampling rate to the according amount for the delay effect


delayy(x,n,d) = x @ min(n, max(0,d));

process = _,ma.SR,4096 : delayy : _,ma.SR,4096 : delayy;
