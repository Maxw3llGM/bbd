######################################################################
# Automatically generated by qmake (3.1) Sun Mar 6 12:27:53 2022
######################################################################

QT += widgets printsupport network
CONFIG+=warn_off
QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.9
INCLUDEPATH+=/usr/local/include
INCLUDEPATH+=
QMAKE_CXXFLAGS= -std=c++11 -Ofast -mmacosx-version-min=10.9 -Wno-unused-parameter 
QMAKE_LFLAGS=
LIBS+=-I/usr/local/include -L/usr/local/lib -ljack -framework CoreMIDI -framework CoreFoundation -framework CoreAudio -framework AudioUnit -framework CoreServices     
HEADERS+=/usr/local/include/faust/gui/QTUI.h
RESOURCES+= /usr/local/include/faust/gui/Styles/Grey.qrc
TEMPLATE = app
TARGET = main
INCLUDEPATH += .

# You can make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# Please consult the documentation of the deprecated API in order to know
# how to port your code away from it.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Input
SOURCES += main.cpp
