
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/miu/Desktop/bbd/software/bbdDelay/src/BBD-delay.cpp" "/Users/miu/Desktop/bbd/software/bbdDelay/cmake-build-debug/CMakeFiles/bbdDelay.dir/src/BBD-delay.cpp.o"
  "/Users/miu/Desktop/bbd/software/bbdDelay/src/Delay.cpp" "/Users/miu/Desktop/bbd/software/bbdDelay/cmake-build-debug/CMakeFiles/bbdDelay.dir/src/Delay.cpp.o"
  "/Users/miu/Desktop/bbd/software/bbdDelay/src/Iir.cpp" "/Users/miu/Desktop/bbd/software/bbdDelay/cmake-build-debug/CMakeFiles/bbdDelay.dir/src/Iir.cpp.o"
  "/Users/miu/Desktop/bbd/software/bbdDelay/src/Noise.cpp" "/Users/miu/Desktop/bbd/software/bbdDelay/cmake-build-debug/CMakeFiles/bbdDelay.dir/src/Noise.cpp.o"
  "/Users/miu/Desktop/bbd/software/bbdDelay/src/RtAudio.cpp" "/Users/miu/Desktop/bbd/software/bbdDelay/cmake-build-debug/CMakeFiles/bbdDelay.dir/src/RtAudio.cpp.o"
  "/Users/miu/Desktop/bbd/software/bbdDelay/src/Stk.cpp" "/Users/miu/Desktop/bbd/software/bbdDelay/cmake-build-debug/CMakeFiles/bbdDelay.dir/src/Stk.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "AppleClang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "__MACOSX_CORE__"
  "__RTMIDI_DEBUG__"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
