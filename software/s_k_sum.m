lp1 = s_k_LPF(0.0068*10^-6,0.082*10^-6,0.00033*10^-6,10000);
lp2 = s_k_LPF(0.0022*10^-6,0.033*10^-6,0.001*10^-6,10000);
lp3 = s_K_LPF2(0.039*10^-6,330*10^-12,10000,10000);

h = bodeplot(lp1,"b-",lp2,"r--",lp3,"g.",{1,100000});
setoptions(h,'FreqUnits','Hz','PhaseVisible','off');
grid on
legend("3^{rd} order Anti Aliasing Fitler", "3^{rd} order reconstruction filter", "2^{nd} order reconstruction filter")

